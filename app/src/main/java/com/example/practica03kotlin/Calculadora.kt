package com.example.practica03kotlin

class Calculadora(var num1: Float = 6f, var num2: Float = 6f) {

    constructor(num1: Int, num2: Int) : this(num1.toFloat(), num2.toFloat())

    fun suma():Float {
        return num1+num2;
    }

    fun resta():Float {
        return num1-num2;
    }

    fun multiplicacion():Float {
        return num1*num2;
    }

    fun division():Float {
        var total: Float = 0f;
        if(num2 != 0f){
            total = num1/num2;
        }
        return total;
    }
}